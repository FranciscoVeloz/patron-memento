﻿using Memento.clases;
using System;

namespace Memento.consola
{
    class Program
    {
        static CareTaker carataker = new CareTaker();
        static void Main(string[] args)
        {
            var p = new Persona();
            p.Nombre = "Francisco";
            carataker.Add(p.saveToMemento());

            p.Nombre = "Daneli";
            carataker.Add(p.saveToMemento());

            p.Nombre = "Juan";
            carataker.Add(p.saveToMemento());

            Memento.clases.Memento m1 = carataker.GetMemento(0);
            Console.WriteLine($"Viendo memento 1: {m1.Estado}");
            Console.WriteLine($"Viendo memento 2: {carataker.GetMemento(1).Estado}");
            Console.WriteLine($"Viendo memento 3: {carataker.GetMemento(2).Estado}");

            p.restoreToMemento(m1);

            Console.ReadKey();
        }
    }
}

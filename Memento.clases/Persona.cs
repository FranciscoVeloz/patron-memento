﻿using System;

namespace Memento.clases
{
    public class Persona
    {
        public string Nombre { get; set; }
        public Memento saveToMemento()
        {
            Console.WriteLine($"Originator: Guadar memento para {Nombre}");
            return new Memento(Nombre);
        }
        public void restoreToMemento(Memento m)
        {
            Nombre = m.Estado;
            Console.WriteLine($"Originator: Recuperando memento {Nombre}");
        }

    }
}
